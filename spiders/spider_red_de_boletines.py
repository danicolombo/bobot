import scrapy

# Otros imports
############################
import json
import re
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose
from scrapy.spiders import Spider
from w3lib.html import remove_tags
from bobot.items import BobotItem

###########################################

# Uso de scrapy para armar data.json

class bobotSpider(scrapy.Spider):
    name = "bobotspider"

    def start_requests(self):
        url_inicial ='http://www.reddeboletines.gob.ar/'

        yield scrapy.Request(url=url_inicial, callback=self.find_items)

    def find_items(self, response):
        urls_en_inicial ='http://www.reddeboletines.gob.ar'
        links = set([urls_en_inicial + i for i in response.xpath('//div[contains(@class, "field-content")]/a/@href').getall()])

        for link in links:

            yield scrapy.Request(url=link, callback=self.find_sites)

    def find_sites(self, response):
        item = BobotItem()
        provincia = response.xpath('//*/h1/text()').extract()
        links_boletines_provinciales = response.xpath('//*[@class="list-group-item"]//a/@href')[0].getall()
        dias_que_sale = response.xpath('//li[8]/text()').getall()
        bo = list(zip(provincia, links_boletines_provinciales, dias_que_sale))

        for x in bo:
            item['provincia'] = x[0]
            item['url'] = x[1]
            item['dias_que_sale'] = x[2]

            yield item
