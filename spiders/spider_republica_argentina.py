import scrapy
import requests
from datetime import datetime, date, time, timedelta

# Otros imports
############################
import json
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose
from scrapy.spiders import Spider
from w3lib.html import remove_tags
#################################

from bobot.items import BobotItem2

###########################################

# Boletin de nacion

def boletin_diario_caba():
    formato = "20%y%m%d"
    hoy = datetime.today()  # Asigna fecha-hora

    # Aplica formato
    fecha_hoy = hoy.strftime(formato)

    url = 'https://documentosboletinoficial.buenosaires.gob.ar/publico/{0}.pdf'.format(fecha_hoy)
    response = requests.get(url)

    with open('./boletines/bo_caba.pdf', 'wb') as f:
        f.write(response.content)

class spider_argentina(scrapy.Spider):
    name = "argentina"

    def start_requests(self):
        with open('data.json') as f:
            data_provincias = json.load(f)

        provincia = [provincia for provincia in data_provincias if provincia['provincia'] == 'República Argentina'][0]
        yield scrapy.Request(url=provincia['url'], callback=self.find_items)



    def find_items(self,response):
            print(response)
