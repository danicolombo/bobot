import scrapy

# Otros imports
############################
import json
from scrapy.loader import ItemLoader
from scrapy.loader.processors import TakeFirst, MapCompose
from scrapy.spiders import Spider
from w3lib.html import remove_tags
#################################

from bobot.items import BobotItem2

###########################################

#TODO: revisar


class spider_misiones(scrapy.Spider):
    name = "misiones"

    def start_requests(self):
        with open('data.json') as f:
            data_provincias = json.load(f)

        provincia = [provincia for provincia in data_provincias if provincia['provincia'] == 'Provincia de Misiones'][0]
        yield scrapy.Request(url=provincia['url'], callback=self.find_items)



    def find_items(self,response):
        print('-+-+-+-+-+-+-+-')
        print(response)
