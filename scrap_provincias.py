from spider_ciudad_autonoma_de_buenos_aires import boletin_diario_caba

def scrap(prov):

    if prov == 'República Argentina':
        boletin_republica_argentina()

    elif prov == 'Ciudad Autónoma de Buenos Aires':
        boletin_diario_caba()

    elif prov == 'Provincia de Buenos Aires':
        boletin_diario_buenos_aires()

    elif prov == 'Provincia de Santa Cruz':
        boletin_diario_santa_cruz()

    elif prov == 'Provincia de Jujuy':
        boletin_diario_jujuy()

    elif prov == 'Provincia de Santa Fe':
        boletin_diario_santa_fe()

    elif prov == 'Provincia de Catamarca':
        boletin_diario_catamarca()

    elif prov == 'Provincia de Córdoba':
        boletin_diario_cordoba()

    elif prov == 'Provincia de La Pampa':
        boletin_diario_la_pampa()

    elif prov == 'Provincia de Salta':
        boletin_diario_salta()

    elif prov == 'Provincia de La Rioja':
        boletin_diario_la_rioja()

    elif prov == 'Provincia de Santiago del Estero':
        boletin_diario_santiago_del_estero()

    elif prov == 'Provincia de Rio Negro':
        boletin_diario_rio_negro()

    elif prov == 'Provincia de Entre Ríos':
        boletin_diario_entre_rios()

    elif prov == 'Provincia de San Juan':
        boletin_diario_san_juan()

    elif prov == 'Provincia del Neuquén':
        boletin_diario_neuquen()

    elif prov == 'Provincia de Tierra del Fuego':
        boletin_diario_tierra_del_fuego()

    elif prov == 'Provincia de Tucumán':
        boletin_diario_tucuman()

    elif prov == 'Provincia de Corrientes':
        boletin_diario_corrientes()

    elif prov == 'Provincia de Misiones':
        boletin_diario_misiones()

    elif prov == 'Provincia de San Luis':
        boletin_diario_san_luis()

    elif prov == 'Provincia de Formosa':
        boletin_diario_formosa()

    elif prov == 'Provincia del Chubut':
        boletin_diario_chubut()

    elif prov == 'Provincia de Mendoza':
        boletin_diario_Mendoza()

    elif prov == 'Provincia del Chaco':
        boletin_diario_chaco()
